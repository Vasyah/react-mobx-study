import { makeAutoObservable } from 'mobx';

class Store {
  counter = 0;
  users = [];
  constructor() {
    makeAutoObservable(this);
  }

  //  actions
  increment() {
    this.counter++;
  }

  decrement() {
    this.counter--;
  }

  reset() {
    this.counter = 0;
  }

  //  computed
  get writeTitleCounter() {
    return `Count value: ${this.counter}`;
  }

  //  async

  fetchUser() {
    fetch('https://jsonplaceholder.typicode.com/posts/1')
      .then((r) => r.json())
      .then((json) => (this.users = json));
  }
}

export default new Store();
