import React from 'react';
import counter from './store/store';
import './App.css';
import { observer } from 'mobx-react';

export const App = observer(() => {
  console.log('App.tsx was rendered');
  console.log(counter.users);
  return (
    <div className={'container'}>
      <div className={'inner'}>
        <div className={'counterContainer'}>
          <span className={'counterText'}>{counter.writeTitleCounter}</span>
        </div>
        <div className={'counterValue'}>{counter.counter}</div>
        <div className={'buttonsGroupContainer'}>
          <button onClick={() => counter.increment()}>Increment by 1</button>
          <button onClick={() => counter.decrement()}>Decrement by 1</button>
          <button onClick={() => counter.reset()}>Reset</button>
        </div>
        <div>
          <p>Fecth users</p> <button onClick={() => counter.fetchUser()}>Fetch it</button>
        </div>
        <div>
          <p>Result of fetching</p>
          <div>{JSON.stringify(counter.users)}</div>
        </div>
      </div>
    </div>
  );
});
